import numpy as np
import json
import pandas

def read_network(filename: str) -> np.ndarray:
    """Takes a json file, reads the weights and returns it as a matrix

    :param filename: the location of the json file
    :type filename: str
    :return: a matrix with all the weights in it
    :rtype: np.ndarray
    """    
    

    with open(filename) as json_file:
        data = json.load(json_file)
    
    #takes all the layer out of the json and adds them to a list so that it is later easier to acces a certain layer.
    layers = [data["layer" + str(layer + 1)] for layer in range(len(data))]
    weight_matrix = []
    ammount_weights = int(layers[0]['size_in']) #Has to be the in for the first layer so that al matrici have the same dimmensions
    
    for layer in range(len(layers)):
        curr_layer = layers[layer]
        weights = curr_layer["weights"]
        weight_of_layer_as_list = convert_weights_to_list(ammount_weights,weights)
        weight_matrix.append(weight_of_layer_as_list)
    
    #Converts the list to numpy
    for i in range(len(weight_matrix)):
        weight_matrix[i] = np.array(weight_matrix[i])
    yyy = np.array(weight_matrix)

    while yyy.size > 1:
        yyy[0] = matrix_product(yyy[0],yyy[1])
    
    return yyy[0]


def convert_weights_to_list(ammount_weights,weights):
    """Takes the dict with the weights in it and converts it to list

    :param ammount_weights: The maximum ammount of weights a neuron can have
    :type ammount_weights: int
    :param weights: a dict with the weights in it for every neuron
    :type weights: dict
    :return: a list with all the weights of a neuron where neuron n has the position n-1 in the list
    :rtype: list
    """    
    

    weight_matrix = []  
    for neuron in weights:
        weight_matrix.append([])
        for weight in range(ammount_weights):
            #Checks if the weight has a value if so adds it to the list otherwise adds zero to the list
            if str(weight + 1) not in weights[neuron].keys():
                weight_matrix[int(neuron) -1].append(0)

            else:
                a = weights[str(neuron)][str(weight + 1)]
                weight_matrix[int(neuron) -1].append(a)  
    
    return weight_matrix


def matrix_product(M: np.ndarray, v: np.ndarray) -> np.ndarray: # TODO

    #Checks if the rows are both the same length and if the lengt is equel to the amount of values in de vector
    #Todo make it work for a matrix with more rows now only works if matrix has two rows
    
    
    
    if (len(M[0]) + len(M[1])) / 2 != len(v): 
        raise ValueError
    
    #Todo a test to check if all the values are numerical
    M = list(M)
    v = list(v)
    r = []
    
    #For every value in a row mulitply that value with the coresponding vector.
    #Add these together and you have your new vector position
    for row in range(len(M)):
        p = 0
        for value in range(len(v)):
            p += M[row][value] * v[value]
        r.append(p)
    

    return np.array(r)


def run_network(filename: str, input_vector: np.ndarray) -> np.ndarray: # TODO
    pass

print(read_network("example-2layer.json"))